
import { NativeModules } from 'react-native';
import UnifiedBanner from './libs/GDTUnifiedBanner';
import NativeExpress from './libs/GDTNativeExpress';
import Splash from './libs/GDTSplash';
const Module = NativeModules.GDTModule;

const GDT = {
	Splash,
    UnifiedBanner,
    NativeExpress,
    Module
};
module.exports = GDT;