//
//  RNGDTRewardVideo.m
//  Gdtad
//
//  Created by liangfengsid on 2020/8/3.
//  Copyright © 2020 Facebook. All rights reserved.
//

#import "RewardVideo.h"
#import "GDTRewardVideoAd.h"
#import "GDTSDKConfig.h"
#import <React/RCTBridge.h>

#import <AVFoundation/AVFoundation.h>

@interface RewardVideo () <GDTRewardedVideoAdDelegate>

@property (nonatomic, strong) GDTRewardVideoAd *rewardVideoAd;
@property (nonatomic, strong) NSString *posID;
@property (nonatomic, strong) RCTResponseSenderBlock onReward;

@end

@implementation RewardVideo

- (instancetype)initWithCallBack:(RCTResponseSenderBlock)callback{
    self.onReward = callback;
    return self;
}

- (void)showRewardVideoAD:(NSString *)posID {
    if (self.rewardVideoAd == nil || ![self.posID isEqualToString:posID]) {
        self.rewardVideoAd = [[GDTRewardVideoAd alloc] initWithPlacementId:posID];
        self.rewardVideoAd.delegate = self;
    }
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.rewardVideoAd loadAd];
    });
    // TODO: without this dispatch, the delegate functions, i.e., gdt_rewardVideoAdDidLoad
    // will not be called due to unknown reasons.
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2000000000 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.rewardVideoAd loadAd];
    });
}

#pragma mark - GDTRewardVideoAdDelegate
- (void)gdt_rewardVideoAdDidLoad:(GDTRewardVideoAd *)rewardedVideoAd
{
    NSLog(@"%s",__FUNCTION__);
    NSLog(@"eCPM:%ld eCPMLevel:%@", [rewardedVideoAd eCPM], [rewardedVideoAd eCPMLevel]);
    
    if (self.rewardVideoAd.expiredTimestamp <= [[NSDate date] timeIntervalSince1970]) {
        NSLog(@"广告已过期，请重新拉取");
        return;
    }
    if (!self.rewardVideoAd.isAdValid) {
        NSLog(@"广告失效，请重新拉取");
        return;
    }
    
    [GDTSDKConfig enableDefaultAudioSessionSetting:false];
    
    UIViewController* viewController = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
    [self.rewardVideoAd showAdFromRootViewController:viewController];
    
    [[AVAudioSession sharedInstance] setActive:NO error:nil];
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
    [[AVAudioSession sharedInstance] setActive:YES error:nil];
}


- (void)gdt_rewardVideoAdVideoDidLoad:(GDTRewardVideoAd *)rewardedVideoAd
{
    NSLog(@"%s",__FUNCTION__);
}


- (void)gdt_rewardVideoAdWillVisible:(GDTRewardVideoAd *)rewardedVideoAd
{
    NSLog(@"%s",__FUNCTION__);
    NSLog(@"视频播放页即将打开");
}

- (void)gdt_rewardVideoAdDidExposed:(GDTRewardVideoAd *)rewardedVideoAd
{
    NSLog(@"%s",__FUNCTION__);
    NSLog(@"广告已曝光");
}

- (void)gdt_rewardVideoAdDidClose:(GDTRewardVideoAd *)rewardedVideoAd
{
    NSLog(@"%s",__FUNCTION__);
    //    广告关闭后释放ad对象
    self.rewardVideoAd.delegate = nil;
    self.rewardVideoAd = nil;
    NSLog(@"广告已关闭");
}


- (void)gdt_rewardVideoAdDidClicked:(GDTRewardVideoAd *)rewardedVideoAd
{
    NSLog(@"%s",__FUNCTION__);
    NSLog(@"广告已点击");
}

- (void)gdt_rewardVideoAd:(GDTRewardVideoAd *)rewardedVideoAd didFailWithError:(NSError *)error
{
    NSLog(@"%s",__FUNCTION__);
    NSString *msg = @"Error";
    if (error.code == 4014) {
        msg = @"请拉取到广告后再调用展示接口";
    } else if (error.code == 4016) {
        msg = @"应用方向与广告位支持方向不一致";
    } else if (error.code == 5012) {
        msg = @"广告已过期";
    } else if (error.code == 4015) {
        msg = @"广告已经播放过，请重新拉取";
    } else if (error.code == 5002) {
        msg = @"视频下载失败";
    } else if (error.code == 5003) {
        msg = @"视频播放失败";
    } else if (error.code == 5004) {
        msg = @"没有合适的广告";
    } else if (error.code == 5013) {
        msg = @"请求太频繁，请稍后再试";
    } else if (error.code == 3002) {
        msg = @"网络连接超时";
    } else if (error.code == 5027){
        msg = @"页面加载失败";
    }
    NSLog(@"ERROR: %@", error);
    if (self.onReward != nil) {
        self.onReward(@[msg]);
    }
}

- (void)gdt_rewardVideoAdDidRewardEffective:(GDTRewardVideoAd *)rewardedVideoAd
{
    NSLog(@"%s",__FUNCTION__);
    NSLog(@"播放达到激励条件");
    if (self.onReward != nil) {
        self.onReward(@[@""]);
    }
}

- (void)gdt_rewardVideoAdDidPlayFinish:(GDTRewardVideoAd *)rewardedVideoAd
{
    NSLog(@"%s",__FUNCTION__);
    NSLog(@"视频播放结束");
    
    [[AVAudioSession sharedInstance] setActive:NO withOptions:AVAudioSessionSetActiveOptionNotifyOthersOnDeactivation error:nil];
}

@end
