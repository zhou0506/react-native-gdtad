//
//  RNGDTRewardVideo.h
//  Gdtad
//
//  Created by liangfengsid on 2020/8/3.
//  Copyright © 2020 Facebook. All rights reserved.
//

#ifndef RewardVideo_h
#define RewardVideo_h

#import <React/RCTBridge.h>

@interface RewardVideo : NSObject

- (instancetype)initWithCallBack:(RCTResponseSenderBlock)callback;
- (void)showRewardVideoAD:(NSString *)posID;

@end

#endif /* RewardVideo_h */
