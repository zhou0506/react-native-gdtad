//
//  RNGDTUnifiedBannerViewManager.m
//  RNGdt
//
//  Created by Steven on 2017/6/14.
//  Copyright © 2017年 Facebook. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RNGDTUnifiedBannerViewManager.h"
#import "RNGDTUnifiedBanner.h"

@implementation RNGDTUnifiedBannerViewManager

// - (dispatch_queue_t)methodQueue {
//     return dispatch_get_main_queue();
// }

 RCT_EXPORT_MODULE(GDTUnifiedBanner);

 RCT_EXPORT_VIEW_PROPERTY(appInfo, NSDictionary);

 RCT_EXPORT_VIEW_PROPERTY(onReceived, RCTBubblingEventBlock);
 RCT_EXPORT_VIEW_PROPERTY(onFailToReceived, RCTBubblingEventBlock);
 RCT_EXPORT_VIEW_PROPERTY(onViewWillLeaveApplication, RCTBubblingEventBlock);
 RCT_EXPORT_VIEW_PROPERTY(onViewWillClose, RCTBubblingEventBlock);
 RCT_EXPORT_VIEW_PROPERTY(onViewWillExposure, RCTBubblingEventBlock);
 RCT_EXPORT_VIEW_PROPERTY(onClicked, RCTBubblingEventBlock);
 RCT_EXPORT_VIEW_PROPERTY(onViewWillPresentFullScreenModal, RCTBubblingEventBlock);
 RCT_EXPORT_VIEW_PROPERTY(onViewDidPresentFullScreenModal, RCTBubblingEventBlock);
 RCT_EXPORT_VIEW_PROPERTY(onViewWillDismissFullScreenModal, RCTBubblingEventBlock);
 RCT_EXPORT_VIEW_PROPERTY(onViewDidDismissFullScreenModal, RCTBubblingEventBlock);

 - (UIView *)view {
     return [[RNGDTUnifiedBanner alloc] init];
 }

@end
