//
//  Gdtad.h
//  RNGdt
//
//  Created by liangfengsid on 2020/7/31.
//  Copyright © 2020 Qmaple. All rights reserved.
//

#ifndef Gdtad_h
#define Gdtad_h

#import <React/RCTBridgeModule.h>

// define share type constants

@interface Gdtad : NSObject <RCTBridgeModule>

@end

#endif /* Gdtad_h */
